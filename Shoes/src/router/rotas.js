import Vue from 'vue'
import VueRouter from 'vue-router'
import Todo from '../components/Todo';
import CadCliente from '../components/CadCliente';
import CadProdutos from '../components/CadProdutos';
import Login from '../components/Login';
import PerfilCliente from '../components/PerfilCliente';
import BuySection from '../components/BuySection';


Vue.use(VueRouter)
export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/cadastro-cliente',
      name: 'cadastroCliente',
      component: CadCliente
    },
    {
      path: '/todo',
      name: 'todo',
      component: Todo
    },
    {
      path: '/cadastro-produto',
      name: 'cadastroProduto',
      component: CadProdutos
    },
    {
      path: '/perfil-cliente',
      name: 'perfilCliente',
      component: PerfilCliente
    },
    {
      path: '/buy-section',
      name: 'buySection',
      component: BuySection
    },


  ]
})