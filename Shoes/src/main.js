import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Login from './components/Login';
import TopBar from './components/TopBar';
import Todo from './components/Todo';
import CadCliente from './components/CadCliente';
import BuySection from './components/BuySection';
import CadProdutos from './components/CadProdutos';
import DeletarProduto from './components/DeletarProduto';
import PerfilCliente from './components/PerfilCliente';
import router from './router/rotas'
import UpdateProduto from './components/UpdateProduto';
import requests from './services/requests'
import Vuelidate from 'vuelidate'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Chat from 'vue-beautiful-chat'
import VueSocketIO from 'vue-socket.io'
import VueFab from 'vue-float-action-button'
import VueJquery from 'vue-jquery'

Vue.use(VueJquery)
Vue.use(Chat)
Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.use(IconsPlugin)
/* Vue.use(VueFab, {
  iconType: 'iconMessage'
}) */
/* Vue.use(new VueSocketIO({
  debug: true,
  connection: 'localhost:3000', //options object is Optional
  vuex: {
    actionPrefix: "SOCKET_",
    mutationPrefix: "SOCKET_"
  }
})
); */

window.vueApp = new Vue({
  el: '#app',
  components: {App,TopBar, Todo, CadCliente, BuySection, CadProdutos, DeletarProduto, UpdateProduto, PerfilCliente},
  router,
  requests,
  data: {

    id:0,
    name:'',
    email:'',
    tokenAuth:'a',
    imgAvatar:'',
    
      
  },
  methods: {
    
  },
  render: h => h(App)
})
