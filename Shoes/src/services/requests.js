import axios from 'axios'
import main from '../main'

export const http = axios.create({
    baseURL: 'http://localhost:3333/'
    /* baseURL: 'http://10.1.4.20:3333/' */
})

export default {

    //todo
    getProducts: (token) => {

        return http.get('products', { headers: { 'Authorization': 'Bearer ' + token } })

    },
    //alterar para rota de compra
    putBuyProduct: (obj, token, result) => {
        return http.put('store/' + obj,
            {
                "quantidade": result,
            },
            { headers: { 'Authorization': 'Bearer ' + token } }
        )
    },

    //login
    putLogin: (email, password) => {
        return http.put('sessions',
            {
                "email": email,
                "password": password

            })
    },


    //produto

    postCadProduct: (token, productBrand, productDesc, productPrice, productQuantity, productImage) => {
        return http.post('products', {

            "modelo": productBrand,
            "descricao": productDesc,
            "preco": productPrice,
            "quantidade": productQuantity,
            "imagem_url": productImage,

        },
            { headers: { 'Authorization': 'Bearer ' + token } }
        )
    },

    deleteProduct: (token, id) => {
        return http.delete('products/' + id,
            { headers: { 'Authorization': 'Bearer ' + token } })

    },

    putUpdateProduct: (token, id, modelo, descricao, preco, quantidade, imgId) => {
        return http.put('products/' + id,
            {
                "modelo": modelo,
                "descricao": descricao,
                "quantidade": quantidade,
                "preco": preco,
                "imagem_id": imgId
            },
            { headers: { 'Authorization': 'Bearer ' + token } }
        )
    },

    getUpdateProduct: (token, id) => {
        return http.get('store/' + id, { headers: { 'Authorization': 'Bearer ' + token } })
    },

    postImage: (token, id, formData) => {
        return http.post('image/' + id, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            }
        })
    },

    getAllProducts: (token) => {
        return http.get('products', { headers: { 'Authorization': 'Bearer ' + token } })
    },

    //cliente

    postCadCliente: (clientName, clientEmail, clientPassword) => {
        return http.post('users', {

            "name": clientName,
            "email": clientEmail,
            "password": clientPassword

        })
    },

    getCliente: (token) => {
        return http.get('users', { headers: { 'Authorization': 'Bearer ' + token } })
    },

    putCliente: (token, nome, email) => {
        return http.put('users', {
            "name": nome,
            "email": email,
        },
            { headers: { 'Authorization': 'Bearer ' + token } }
        )
    },

    putClientePass: (token, nome, email, oldPass, newPass, conPass) => {
        return http.put('users', {
            "name": nome,
            "email": email,
            "oldPassword": oldPass,
            "password": newPass,
            "confirmPassword": conPass
        },
            { headers: { 'Authorization': 'Bearer ' + token } }
        )
    },

    deleteCliente: (token) => {
        return http.delete('users',
            { headers: { 'Authorization': 'Bearer ' + token } })
    },

    postImageCliente: (token, formData) => {
        return http.post('avatar', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            }
        })
    },

    // Notificiations

    getNotifications: (token) => {
        return http.get('notifications', { headers: { 'Authorization': 'Bearer ' + token } })
    },

    putNotification: (token, id) => {
        return http.put('notifications/' + id, {}, { headers: { 'Authorization': 'Bearer ' + token } })
    },

    // Chat

    getProviders: (token) => {
        return http.get('providers', {headers: {'Authorization': 'Bearer ' + token}})
    },

    getUsers: (token) => {
        return http.get('users-all',{headers: {'Authorization': 'Bearer ' + token}})
    },

    getNotificationsChat: (token) => {
        return http.get('message-notifications',{headers: {'Authorization': 'Bearer ' + token}})
    }

}